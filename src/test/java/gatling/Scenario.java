package gatling;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;

public class Scenario {
    private final Step step;

    public Scenario() {
        this.step = new Step();
    }

    public ScenarioBuilder createScenarios() {
        return CoreDsl.scenario("Main scenarios")
                .exec(step.createTask())
                .exec(step.getTaskById())
                .exec(step.updateTask())
                .exec(step.getAllTasks())
                .exec(step.deleteTask());
    }
}