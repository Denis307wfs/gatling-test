package gatling;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpDsl;
import io.gatling.javaapi.http.HttpProtocolBuilder;

public class GatlingSimulation extends Simulation {
    private static final String BASE_URL = "http://localhost:9092";
    private static final String USER_NAME = "Mark";
    private static final String BASE_PASSWORD = "123";
    private static final int users = 10;
    private static final int during = 10;

    public GatlingSimulation() {
        HttpProtocolBuilder httpProtocol =
                HttpDsl.http.baseUrl(BASE_URL)
                        .basicAuth(USER_NAME, BASE_PASSWORD)
                        .header("Content-Type", "application/json");

        Scenario scenario = new Scenario();
        this.setUp(
                scenario.createScenarios().injectOpen(
                        CoreDsl.constantUsersPerSec(users).during(during)
                )
        ).protocols(httpProtocol);
    }
}