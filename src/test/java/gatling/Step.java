package gatling;

import io.gatling.javaapi.core.ChainBuilder;
import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.http.HttpDsl;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static io.gatling.javaapi.core.CoreDsl.StringBody;
import static io.gatling.javaapi.core.CoreDsl.jsonPath;

public class Step {
    private final Iterator<Map<String, Object>> taskMap;
    private final Iterator<Map<String, Object>> taskStatusMap;
    private static final String CREATE_TASK = """
            {
                "name": "#{name}",
                "plannedTime": "2023-12-21 18:00"
            }
            """;
    private static final String UPDATE_TASK = """
            {
                "name": "WORK",
                "plannedTime": "2023-12-21 18:00",
                "status": "#{status}"
            }
            """;

    private static final Set<String> SET_OF_STATUS =
            Set.of("PLANNED", "WORK_IN_PROGRESS", "POSTPONED", "NOTIFIED", "SIGNED", "DONE", "CANCELLED");

    private static final int SET_SIZE = SET_OF_STATUS.size();

    public Step() {
        this.taskMap = taskMap();
        this.taskStatusMap = taskStatusMap();
    }

    private Iterator<Map<String, Object>> taskMap() {
        return Stream.generate((Supplier<Map<String, Object>>)
                        () -> Collections.singletonMap("name", RandomStringUtils.randomAlphanumeric(10, 20)))
                .iterator();
    }

    private Iterator<Map<String, Object>> taskStatusMap() {
        return Stream.generate((Supplier<Map<String, Object>>)
                        () -> Collections.singletonMap("status",
                                SET_OF_STATUS.stream().skip(new Random().nextInt(SET_SIZE)).findFirst().orElse(null)))
                .iterator();
    }

    public ChainBuilder createTask() {
        return CoreDsl
                .feed(taskMap)
                .exec(
                        HttpDsl
                                .http("Post new task")
                                .post("/tasks")
                                .body(StringBody(CREATE_TASK))
                                .asJson()
                                .check(HttpDsl.status().is(201),
                                        jsonPath("$.name").is(session -> session.get("name")))
                                .check(jsonPath("$.id").saveAs("id"))
                ).pause(2);
    }

    public ChainBuilder getAllTasks() {
        return CoreDsl.exec(
                HttpDsl
                        .http("Get all task")
                        .get("/tasks")
                        .check(HttpDsl.status().is(200))
        ).pause(2);
    }

    public ChainBuilder getTaskById() {
        return CoreDsl.exec(
                HttpDsl
                        .http("Get task by ID")
                        .get("/tasks/#{id}")
                        .check(HttpDsl.status().is(200))
        ).pause(2);
    }

    public ChainBuilder updateTask() {
        return CoreDsl
                .feed(taskStatusMap)
                .exec(
                        HttpDsl
                                .http("Update task")
                                .put("/tasks/#{id}")
                                .body(StringBody(UPDATE_TASK))
                                .asJson()
                                .check(HttpDsl.status().is(200),
                                        jsonPath("$.status").is(session -> session.get("status")))
                ).pause(2);
    }

    public ChainBuilder deleteTask() {
        return CoreDsl.exec(
                HttpDsl
                        .http("Delete task")
                        .delete("/tasks/#{id}")
                        .check(HttpDsl.status().is(200))
        ).pause(2);
    }
}